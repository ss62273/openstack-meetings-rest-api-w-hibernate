package assign.resources;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import assign.domain.Meeting;
import assign.domain.Project;
import assign.services.DBLoader;


/**
 * 
 * CS 378 Modern Web Applications -<br>
 * RESTEasy & Hibernate
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

@Path("/projects")
public class MyEavesdropResource {

  DBLoader dbLoader;

  public MyEavesdropResource(@Context ServletContext servletContext) {
    this.dbLoader = new DBLoader();
  }

  @POST
  @Path("/")
  @Consumes("application/xml")
  public Response createProject(Project project) throws Exception {

    // Return 400 Bad Request: invalid project parameter
    if (project == null || project.getName() == null || project.getName().trim().length() < 1
        || project.getDescription() == null || project.getDescription().trim().length() < 1) {
      return Response.status(Response.Status.BAD_REQUEST).build();
    }

    final Project newProject = dbLoader.addProject(project);
    if (newProject == null) {
      // Should not reach here
      Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    final URI path = URI.create("/projects/" + newProject.getId());

    // 201 Created
    return Response.created(path).build();
  }

  @POST
  @Path("/{project_id}/meetings")
  @Consumes("application/xml")
  public Response createMeeting(@PathParam("project_id") Long project_id, Meeting meeting)
      throws Exception {

    // Return 400 Bad Request: invalid project parameter
    if (meeting == null || meeting.getName() == null || meeting.getName().trim().length() < 1
        || meeting.getYear() == null || meeting.getYear().trim().length() < 1
        || Integer.parseInt(meeting.getYear().trim()) < 2010
        || Integer.parseInt(meeting.getYear().trim()) > 2017) {
      return Response.status(Response.Status.BAD_REQUEST).build();
    }

    final Project project = dbLoader.getProject(project_id);
    // Return 404 Not Found: project with provided project id does not exist
    if (project == null) {
      return Response.status(Response.Status.NOT_FOUND).build();
    }

    final Meeting newMeeting = dbLoader.addMeeting(meeting, project);
    if (newMeeting == null) {
      Response.status(Response.Status.BAD_REQUEST).build();
    }

    // 201 Created
    return Response.status(Response.Status.CREATED).build();
  }

  @GET
  @Path("/{project_id}")
  @Produces("application/xml")
  public Response getProject(@PathParam("project_id") Long project_id) throws Exception {
    // Return 404 Not Found: invalid project_id is passed in
    if (project_id == null || project_id < 0) {
      return Response.status(Response.Status.NOT_FOUND).build();
    }

    final Project project = dbLoader.getProject(project_id);
    // Return 404 Not Found: project with provided project id does not exist
    if (project == null) {
      return Response.status(Response.Status.NOT_FOUND).build();
    }

    StreamingOutput stream = new StreamingOutput() {
      public void write(OutputStream outputStream) throws IOException, WebApplicationException {
        outputProject(outputStream, project);
      }
    };

    // 200 OK: Request successful
    return Response.ok(stream).build();
  }

  @PUT
  @Path("/{project_id}/meetings/{meeting_id}")
  @Consumes("application/xml")
  public Response updateMeeting(@PathParam("project_id") Long project_id,
      @PathParam("meeting_id") Long meeting_id, Meeting updateMeeting) throws Exception {
    // Return 400 Bad Request: invalid parameters are passed in
    if (updateMeeting == null || updateMeeting.getName() == null
        || updateMeeting.getName().trim().length() < 1 || updateMeeting.getYear() == null
        || updateMeeting.getYear().trim().length() < 1
        || Integer.parseInt(updateMeeting.getYear().trim()) < 2010
        || Integer.parseInt(updateMeeting.getYear().trim()) > 2017) {
      return Response.status(Response.Status.BAD_REQUEST).build();
    }

    final Project project = dbLoader.getProject(project_id);
    // Return 404 Not Found: project with provided project id does not exist
    if (project == null) {
      return Response.status(Response.Status.NOT_FOUND).build();
    }

    final Meeting newMeeting = dbLoader.getMeeting(meeting_id);
    if (newMeeting == null) {
      // Return 404 Not Found: meeting with provided meeting id does not exist
      return Response.status(Response.Status.NOT_FOUND).build();
    }

    dbLoader.updateMeeting(updateMeeting, meeting_id);

    // 200 OK: Request successful
    return Response.ok().build();
  }

  @DELETE
  @Path("/{project_id}")
  @Produces("application/xml")
  public Response deleteProject(@PathParam("project_id") Long project_id) throws Exception {
    // Return 404 Not Found: invalid project_id is passed in
    if (project_id == null || project_id < 0) {
      return Response.status(Response.Status.NOT_FOUND).build();
    }

    final Project project = dbLoader.getProject(project_id);
    // Return 404 Not Found: project with project_id does not exist
    if (project == null) {
      return Response.status(Response.Status.NOT_FOUND).build();
    }

    dbLoader.deleteProject(project_id);

    // 200 OK: Request successful
    return Response.ok().build();
  }

  protected void outputProject(OutputStream os, Project project) throws IOException {
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(Project.class);
      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      jaxbMarshaller.marshal(project, os);
    } catch (JAXBException jaxb) {
      jaxb.printStackTrace();
      throw new WebApplicationException();
    }
  }
}
