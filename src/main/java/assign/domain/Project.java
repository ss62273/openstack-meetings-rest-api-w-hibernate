package assign.domain;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 5 Hibernate
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

@Entity
@Table(name = "project")
@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.FIELD)
public class Project {

  @XmlAttribute
  private Long id;
  private String name;
  private String description;
  @XmlElementWrapper(name = "meetings")
  private Set<Meeting> meeting;

  public Project() {
    // this form used by Hibernate
  }

  public Project(String name, String description) {
    // for application use, to create new project
    this.name = name;
    this.description = description;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return this.id;
  }

  @SuppressWarnings("unused")
  private void setId(Long id) {
    // Project ID will be set by the database
    this.id = id;
  }

  @Column(name = "name")
  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Column(name = "description")
  public String getDescription() {
    return this.description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @OneToMany(mappedBy = "project")
  @Cascade({CascadeType.DELETE})
  public Set<Meeting> getMeetings() {
    return this.meeting;
  }

  public void setMeetings(Set<Meeting> meetings) {
    this.meeting = meetings;
  }
}
