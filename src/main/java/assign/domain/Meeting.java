package assign.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 5 Hibernate
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

@Entity
@Table(name = "meeting")
@XmlRootElement(name = "meeting")
@XmlAccessorType(XmlAccessType.FIELD)
public class Meeting {

  @XmlAttribute
  private Long id;
  private String name;
  private String year;
  @XmlTransient
  private Project project;

  public Meeting() {
    // this form used by Hibernate
  }

  public Meeting(String name, String year) {
    // for application use, to create new meeting
    this.name = name;
    this.year = year;
  }

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  @Column(name = "name")
  public String getName() {
    return this.name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Column(name = "year")
  public String getYear() {
    return this.year;
  }

  public void setYear(String year) {
    this.year = year;
  }

  @ManyToOne
  @JoinColumn(name = "project_id")
  public Project getProject() {
    return this.project;
  }

  public void setProject(Project p) {
    this.project = p;
  }
}
