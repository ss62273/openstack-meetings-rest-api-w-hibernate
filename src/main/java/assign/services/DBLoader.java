package assign.services;

import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import assign.domain.Meeting;
import assign.domain.Project;
import java.util.logging.*;

/**
 * 
 * CS 378 Modern Web Applications -<br>
 * Assignment 5 Hibernate
 * 
 * @author Sangjin Shin<br>
 *         UTEID: ss62273<br>
 *         CSID: sshin96<br>
 *         Email: sangjinshin@outlook.com
 * 
 */

public class DBLoader {
  private SessionFactory sessionFactory;

  Logger logger;

  @SuppressWarnings("deprecation")
  public DBLoader() {
    // A SessionFactory is set up once for an application
    sessionFactory = new Configuration().configure() // configures settings from hibernate.cfg.xml
        .buildSessionFactory();

    logger = Logger.getLogger("DBLoader");
  }

  public void loadData(Map<String, List<String>> data) {
    logger.info("Inside loadData.");
  }

  public Project addProject(Project p) throws Exception {
    Session session = sessionFactory.openSession();
    Transaction tx = null;
    Project project = null;

    try {
      tx = session.beginTransaction();
      project = new Project(p.getName(), p.getDescription());
      session.save(project);
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
        throw e;
      }
    } finally {
      session.close();
    }

    return project;
  }

  public Meeting addMeeting(Meeting m, Project p) throws Exception {
    Session session = sessionFactory.openSession();
    Transaction tx = null;
    Meeting meeting = null;

    try {
      tx = session.beginTransaction();
      meeting = new Meeting(m.getName(), m.getYear());
      meeting.setId(m.getId());
      meeting.setProject(p);
      p.getMeetings().add(m);
      session.save(meeting);
      tx.commit();
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
        throw e;
      }
    } finally {
      session.close();
    }

    return meeting;
  }

  public Project getProject(Long projectId) throws Exception {
    Session session = sessionFactory.openSession();
    session.beginTransaction();

    Criteria criteria =
        session.createCriteria(Project.class).add(Restrictions.eq("id", projectId));

    @SuppressWarnings("unchecked")
    List<Project> projects = criteria.list();

    if (projects.size() > 0) {
      Hibernate.initialize(projects.get(0).getMeetings());
      session.close();
      return projects.get(0);
    } else {
      session.close();
      return null;
    }
  }

  public Meeting getMeeting(Long meetingId) throws Exception {
    Session session = sessionFactory.openSession();
    session.beginTransaction();

    Criteria criteria =
        session.createCriteria(Meeting.class).add(Restrictions.eq("id", meetingId));

    @SuppressWarnings("unchecked")
    List<Meeting> meetings = criteria.list();

    session.close();

    if (meetings.size() > 0) {
      return meetings.get(0);
    } else {
      return null;
    }
  }

  public void deleteProject(Long projectId) throws Exception {
    Session session = sessionFactory.openSession();
    session.beginTransaction();
    String query = "from Project p where p.id = :id";

    Project p =
        (Project) session.createQuery(query).setParameter("id", projectId).list().get(0);

    session.delete(p);

    session.getTransaction().commit();
    session.close();
  }

  public void updateMeeting(Meeting meeting, Long meetingId) throws Exception {
    Session session = sessionFactory.openSession();
    Transaction tx = null;
    Meeting m = null;
    String query = "from Meeting m where m.id = :id";

    try {
      tx = session.beginTransaction();
      m = (Meeting) session.createQuery(query).setParameter("id", meetingId).list().get(0);
      if (m != null) {
        m.setName(meeting.getName());
        m.setYear(meeting.getYear());
        session.update(m);
        tx.commit();
      }
    } catch (Exception e) {
      if (tx != null) {
        tx.rollback();
        throw e;
      }
    } finally {
      session.close();
    }
  }
}
